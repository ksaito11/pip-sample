from . import hellomodule

hello = hellomodule.hello()

def cli_hello():
    return hello.msg()

if __name__ == '__main__':
    print('{0}'.format(cli_hello()))
