import os

GREETING = os.environ.get('HELLO_GREETING', 'Hello')

class hello:
    def __init__(self):
        self.hello_msg = os.environ.get('HELLO_MSG', 'World!!')

    def msg(self):
        return '{0}, {1}'.format(GREETING, self.hello_msg)

if __name__ == '__main__':
    print('export HELLO_GREETING=<greeting>')
    print('export HELLO_MSG=<msg>')
    print('Usage: python hello/hello.py')
    hello = hello()
    print('hello {0}'.format(hello.msg()))
