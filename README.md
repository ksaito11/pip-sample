# pip-sample

pipパッケージのサンプルです。

## 要件

* direnv
* python3
* pip
* wheel

## インストール

```
pip install git+ssh://git@gitlab.com/ksaito11/pip-sample.git@v1.0
```

## 使い方

```
$ piphello
Hello, World!!
$ HELLO_GREETING=hi piphello
hi, World!!
$ 
```

## ライブラリとして呼び出し

```
 python
Python 3.8.2 (default, Apr 27 2020, 15:53:34) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from hellopkg import hello
>>> hello.msg()
'Hello, World!!'
>>> 
```

## パッケージ

下記のコマンドでwheelフォーマットのパッケージが``dist/``に作成されます。

```
python setup.py bdist_wheel
```

下記のコマンドでインストールできます。

```
pip install dist/hello-*.whl
```

## 参考情報

[setuptools_scm](https://github.com/pypa/setuptools_scm)

以上
